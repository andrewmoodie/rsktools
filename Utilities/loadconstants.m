% loadconstants - A collection of constants to be used in RSKtools
%
% Author: RBR Ltd. Ottawa ON, Canada
% email: support@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2018-11-08

latestRSKversion = '2.5.8';
latestRSKversionMajor = 2;
latestRSKversionMinor = 5;
latestRSKversionPatch = 8;

RSKtoolsversion = '3.0.0';
RSKtoolsversionMajor = 3;
RSKtoolsversionMinor = 0;
RSKtoolsversionPatch = 0;

eventBeginUpcast = 33;
eventBeginDowncast = 34;
eventEndcast = 35;

defaultpAtm = 10.1325;
