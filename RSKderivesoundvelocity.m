function [RSK] = RSKderivesoundvelocity(RSK)

% RSKderivesoundvelocity - Calculate sound velocity.
%
% Syntax:  [RSK] = RSKderivesoundvelocity(RSK)
% 
% Derives sound velocity in water using . The result is added to the
% RSK data structure, and the channel list is updated. 
%
% Inputs: 
%    RSK - Structure containing the logger metadata and data.         
%
% Outputs:
%    RSK - Updated structure containing sound velocity.
%
% Author: Andrew J. Moodie
% email: amoodie@rice.edu
% Last revision: 2019-01-21

% identify column index
Tcol = getchannelindex(RSK, 'Temperature');
Scol = getchannelindex(RSK, 'Salinity');

% create new column and index
RSK = addchannelmetadata(RSK, 'sv_00', 'Sound Velocity', 'm/s');
SVcol = getchannelindex(RSK, 'Sound Velocity');

% convert a sea pressure column to bars for formula
[RSKsp, SPcol] = getseapressure(RSK);
castidx = getdataindex(RSK);
for ndx = castidx
    RSKsp.data(ndx).values(:, SPcol) = RSKsp.data(ndx).values(:, SPcol) ./ 10;
end

% calculate and store sound velocity
castidx = getdataindex(RSK);
for ndx = castidx
    soundvelocity = UNESCO_formula(RSK.data(ndx).values(:, Tcol), ...
                                   RSK.data(ndx).values(:, Scol), ...
                                   RSKsp.data(ndx).values(:, SPcol));
    RSK.data(ndx).values(:, SVcol) = soundvelocity;
end

% update log
logentry = ('Sound velocity derived using the UNESCO standard formula.');
RSK = RSKappendtolog(RSK, logentry);


end


function [c] = UNESCO_formula(T, S, P)
%UNESCO_formula UNESCO formula for sound velocity
%
% UNESCO formula for calculating the sound velocity as a
% function of temperature, salinity, and pressure
%   
% T = temperature in degrees Celsius
% S = salinity in Practical Salinity Units (parts per thousand)
% P = pressure in bar
% 
% Range of validity: temperature 0 to 40 °C, 
%                    salinity 0 to 40 parts per thousand, 
%                    pressure 0 to 1000 bar
%                    (Wong and Zhu, 1995).
%
% Formula found from: 
%   http://resource.npl.co.uk/acoustics/techguides/soundseawater/content.html
%

C00 = 1402.388;
C01 = 5.03830;
C02 = -5.81090E-2;
C03 = 3.3432E-4;
C04 = -1.47797E-6;
C05 = 3.1419E-9;
C10 = 0.153563;
C11 = 6.8999E-4;
C12 = -8.1829E-6;
C13 = 1.3632E-7;
C14 = -6.1260E-10;
C20 = 3.1260E-5;
C21 = -1.7111E-6;
C22 = 2.5986E-8;
C23 = -2.5353E-10;
C24 = 1.0415E-12;
C30 = -9.7729E-9;
C31 = 3.8513E-10;
C32 = -2.3654E-12;
A00 = 1.389;
A01 = -1.262E-2;
A02 = 7.166E-5;
A03 = 2.008E-6;
A04 = -3.21E-8;
A10 = 9.4742E-5;
A11 = -1.2583E-5;
A12 = -6.4928E-8;
A13 = 1.0515E-8;
A14 = -2.0142E-10;
A20 = -3.9064E-7;
A21 = 9.1061E-9;
A22 = -1.6009E-10;
A23 = 7.994E-12;
A30 = 1.100E-10;
A31 = 6.651E-12;
A32 = -3.391E-13;
B00 = -1.922E-2;
B01 = -4.42E-5;
B10 = 7.3637E-5;
B11 = 1.7950E-7;
D00 = 1.727E-3;
D10 = -7.9836E-6;

Cw = (C00 + C01.*T + C02.*T.^2 + C03.*T.^3 + C04.*T.^4 + C05.*T.^5) + ...
 	(C10 + C11.*T + C12.*T.^2 + C13.*T.^3 + C14.*T.^4).*P + ...
 	(C20 +C21.*T +C22.*T.^2 + C23.*T.^3 + C24.*T.^4).*P.^2 + ...
 	(C30 + C31.*T + C32.*T.^2).*P.^3;
 
A = (A00 + A01.*T + A02.*T.^2 + A03.*T.^3 + A04.*T.^4) + ...
 	(A10 + A11.*T + A12.*T.^2 + A13.*T.^3 + A14.*T.^4).*P + ...
 	(A20 + A21.*T + A22.*T.^2 + A23.*T.^3).*P.^2 + ...
 	(A30 + A31.*T + A32.*T.^2).*P.^3;
 
B =	B00 + B01.*T + (B10 + B11.*T).*P;
 
D =	D00 + D10.*P;

c = Cw + A.*S + B.*S.^(3/2) + D.*S.^2;

end


